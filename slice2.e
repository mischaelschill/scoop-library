note
	description: "A {SLICE2} is a two-dimensional slice."
	author: "Mischael Schill"
	date: "$Date$"
	revision: "$Revision$"

class
	SLICE2[G]

inherit

	SPARSE_SLICE2[G]
	undefine
		is_equal, is_empty, copy
	end


create
	make_empty,
	make_from_array,
	make_filled,
	slice_top,
	slice_bottom,
	slice_left,
	slice_right,
	merge

feature {NONE} -- Initialization
	make_empty
		do
			empty
		ensure
			empty: rows = 0 and columns = 0
			first_row = 1
			first_column = 1
			outer_rows = rows
			outer_columns = columns
		end

	make_filled (a_default: G; a_rows, a_columns: INTEGER)
		require
			positive_area: a_rows > 0 and a_columns > 0
		do
			first_row := 1
			first_column := 1
			last_row := a_rows
			last_column := a_columns
			row_base := 1
			column_base := 1
			outer_rows := rows
			outer_columns := columns
			create area.make_filled (a_default, rows * columns)
		ensure
			full: rows = a_rows and columns = a_columns
			first_row = 1
			first_column = 1
			filled: filled_with (a_default)
		end

	make_from_array (a_values: separate ARRAY2[G])
		note
			ignore_scoop_check: True
		local
			a: SPECIAL[G]
		do
			a := a_values.area
			first_row := a_values.lower
			first_column := a_values.lower
			last_row := a_values.upper
			last_column := a_values.upper
			row_base := first_row
			column_base := first_column
			outer_rows := rows
			outer_columns := columns
			create area.make_empty (count)
			area.copy_data (a, 0, 0, count)
		end

	slice_top (n: INTEGER; a_original: separate like Current)
		-- Slices off the first `n' rows from `a_original'
		note
			ignore_scoop_check: True
		require
			within_bounds: n >= 0 and n <= a_original.rows
			original_is_modifiable: not a_original.is_frozen
		do
			first_row := a_original.first_row
			first_column := a_original.first_column
			last_row := a_original.first_row + n - 1
			last_column := a_original.last_column
			row_base := a_original.row_base
			column_base := a_original.column_base
			area := a_original.area
			outer_rows := a_original.outer_rows
			outer_columns := a_original.outer_columns
			a_original.hide_top	(n)
		ensure
			a_original.first_row = old a_original.first_row + n
			first_row = old a_original.first_row
			first_column = a_original.first_column
			last_row = a_original.first_row - 1
			last_column = a_original.last_column
		end

	slice_bottom (n: INTEGER; a_original: separate like Current)
		-- Slices off the last `n' rows from `a_original'
		note
			ignore_scoop_check: True
		require
			within_bounds: n >= 0 and n <= a_original.rows
			original_is_modifiable: not a_original.is_frozen
		do
			first_row := a_original.last_row - n + 1
			first_column := a_original.first_column
			last_row := a_original.last_row
			last_column := a_original.last_column
			row_base := a_original.row_base
			column_base := a_original.column_base
			area := a_original.area
			outer_rows := a_original.outer_rows
			outer_columns := a_original.outer_columns
			a_original.hide_bottom (n)
		ensure
			a_original.last_row = old a_original.last_row - n
			first_row = a_original.last_row + 1
			first_column = a_original.first_column
			last_row = old a_original.last_row
			last_column = a_original.last_column
		end

	slice_left (n: INTEGER; a_original: separate like Current)
		-- Slices off the first `n' columns from `a_original'
		note
			ignore_scoop_check: True
		require
			within_bounds: n >= 0 and n <= a_original.columns
			original_is_modifiable: not a_original.is_frozen
		do
			first_row := a_original.first_row
			first_column := a_original.first_column
			last_row := a_original.last_row
			last_column := a_original.first_column + n - 1
			row_base := a_original.row_base
			column_base := a_original.column_base
			area := a_original.area
			outer_rows := a_original.outer_rows
			outer_columns := a_original.outer_columns
			a_original.hide_left (n)
		ensure
			a_original.first_row = old a_original.first_row + n
			first_row = a_original.first_row
			first_column = old a_original.first_column
			last_row = a_original.last_row
			last_column = a_original.last_column - 1
		end

	slice_right (n: INTEGER; a_original: separate like Current)
		-- Slices off the last `n' columns from `a_original'
		note
			ignore_scoop_check: True
		require
			within_bounds: n >= 0 and n <= a_original.columns
			original_is_modifiable: not a_original.is_frozen
		do
			first_row := a_original.first_row
			first_column := a_original.last_column - n + 1
			last_row := a_original.last_row
			last_column := a_original.last_column
			row_base := a_original.row_base
			column_base := a_original.column_base
			area := a_original.area
			outer_rows := a_original.outer_rows
			outer_columns := a_original.outer_columns
			a_original.hide_right (n)
		ensure
			a_original.first_row = old a_original.first_row + n
			first_row = a_original.first_row
			first_column = a_original.last_column + 1
			last_row = a_original.last_row
			last_column = old a_original.last_column
		end

	merge (a_one, a_another: separate SLICE2[G])
		note
			ignore_scoop_check: True
		require
			adjacent: a_one.is_adjacent (a_another)
		local
			i, j, m, p, q, c: INTEGER
			a: SPECIAL[G]
		do
			if a_one.is_top_of (a_another) then
				first_column := a_one.first_column
				last_column := a_one.last_column
				first_row := a_one.first_row
				last_row := a_another.last_row
			elseif a_one.is_bottom_of (a_another) then
				first_column := a_one.first_column
				last_column := a_one.last_column
				first_row := a_another.first_row
				last_row := a_one.last_row
			elseif a_one.is_left_of (a_another) then
				first_column := a_one.first_column
				last_column := a_another.last_column
				first_row := a_one.first_row
				last_row := a_one.last_row
			else
				first_column := a_another.first_column
				last_column := a_one.last_column
				first_row := a_one.first_row
				last_row := a_one.last_row
			end
			if
				a_one.area = a_another.area and
				a_one.row_base = a_another.row_base and
				a_one.column_base = a_another.column_base
			then
				area := a_one.area
				row_base := a_one.row_base
				column_base := a_another.column_base
				outer_rows := a_one.outer_rows
				outer_columns := a_one.outer_columns
				a_another.empty
				a_one.empty
			else
				row_base := first_row
				column_base := first_column
				outer_rows := rows
				outer_columns := columns
				create area.make_empty (rows * columns)
				p := a_one.first_row.min (a_another.first_row)
				from
					a := a_one.area

					i := a_one.first_column - column_base
					m := (last_row - row_base) * outer_columns

					q := a_one.outer_columns
					j := (a_one.first_row - a_one.row_base) * q + first_column - a_one.column_base

					c := a_one.columns
				until
					i > m
				loop
					area.copy_data (a, j, i, c)
					j := j + q
					i := i + outer_columns
				end

				from
					a := a_another.area

					i := a_another.first_column - column_base
					m := (last_row - row_base) * outer_columns

					q := a_another.outer_columns
					j := (a_another.first_row - a_another.row_base) * q + first_column - a_another.column_base

					c := a_another.columns
				until
					i > m
				loop
					area.copy_data (a, j, i, c)
					j := j + q
					i := i + outer_columns
				end
				a_another.empty
				a_one.empty
			end
		end

feature -- Access
	item alias "[]" (i, j: INTEGER): G assign put
			-- Entry at index (`i',`j'), if in index interval
		do
			Result := area.item ((i - row_base) * outer_columns + (j - column_base))
		end

	original: SLICE2[G]
		do
			Result := Current
		end

feature -- Status report
	readers: INTEGER

	first_row, first_column: INTEGER
	last_row, last_column: INTEGER

	valid_indexes (i, j: INTEGER): BOOLEAN
			-- Is (`i',`j') within the bounds of the array?
		do
			Result := (first_row <= i) and (i <= last_row) and (first_column <= j) and (j <= last_column)
		end

	filled_with (a_value: G): BOOLEAN
		local
			i, m: INTEGER
		do
			from
				Result := True
				i := first_row
				m := last_row
			until
				not Result or i > m
			loop
				Result := area.filled_with (a_value,
					(i - row_base) * outer_columns + (first_column - column_base),
					(i - row_base) * outer_columns + (last_column - column_base))
				i := i + 1
			end
		end

	is_empty: BOOLEAN
		do
			Result := count = 0
		end

	has (v: G): BOOLEAN
			-- Does `v' appear in array?
 			-- (Reference or object equality,
			-- based on `object_comparison'.)
		local
			i, j, m, n: INTEGER
		do
			if object_comparison then
				from
					Result := True
					i := first_row
					m := last_row
				until
					not Result or i > m
				loop
					from
						j := first_column
						n := last_column
					until
						not Result or j > n
					loop
						Result := item (i, j) ~ v
						j := j + 1
					end
					i := i + 1
				end
			else
				from
					Result := True
					i := first_row
					m := last_row
				until
					not Result or i > m
				loop
					from
						j := first_column
						n := last_column
					until
						not Result or j > n
					loop
						Result := item (i, j) = v
						j := j + 1
					end
					i := i + 1
				end
			end
		end

	count: INTEGER
		do
			Result := rows * columns
		end

feature -- Comparison

	is_equal (other: like Current): BOOLEAN
			-- Is array made of the same items as `other'?
		local
			i, j, m, n: INTEGER
		do
			if other = Current then
				Result := True
			elseif is_comparable_with (other) then
				if object_comparison then
					from
						Result := True
						i := first_row
						m := last_row
					until
						not Result or i > m
					loop
						from
							j := first_column
							n := last_column
						until
							not Result or j > n
						loop
							Result := item (i, j) ~ other.item (i, j)
							j := j + 1
						end
						i := i + 1
					end
				else
					from
						Result := True
						i := first_row
						m := last_row
					until
						not Result or i > m
					loop
						from
							j := first_column
							n := last_column
						until
							not Result or j > n
						loop
							Result := item (i, j) = other.item (i, j)
							j := j + 1
						end
						i := i + 1
					end
				end
			end
		end

	is_comparable_with (other: separate like Current): BOOLEAN
		do
			Result := first_row = other.first_row and last_row = other.last_row
				and first_column = other.first_column and last_column = other.last_column and
				object_comparison = other.object_comparison
		end

feature -- Element change

	put (v: like item; i, j: INTEGER)
			-- Replace (`i',`j')-th entry, if in index interval, by `v'.
		require else
			within_bounds: i >= first_row and i <= last_row and j >= first_column and j <= last_column
			modifiable: not is_frozen
		do
			area.put (v, (i - row_base) * outer_columns + (j - column_base))
		end

	empty
		do
			first_row := 1
			first_column := 1
			last_row := 0
			last_column := 0
			row_base := 1
			column_base := 1
			outer_rows := rows
			outer_columns := columns
			create area.make_empty (0)
		ensure then
			empty: rows = 0 and columns = 0
			first_row = 1
			first_column = 1
		end

	copy (other: like Current)
			-- Reinitialize by copying all the items of `other'.
			-- (This is also used by `clone'.)
		local
			i, j, m, n: INTEGER
		do
			if not is_frozen then
				first_row := other.first_row
				last_row := other.last_row
				first_column := other.first_column
				last_column := other.last_column
				row_base := first_row
				column_base := first_column
				outer_rows := rows
				outer_columns := columns
				create area.make_empty (count)
				from
					i := first_row
					m := last_row
				until
					i > m
				loop
					from
						j := first_column
						n := last_column
					until
						j > n
					loop
						put (other.item (i, j), i, j)
						j := j + 1
					end
					i := i + 1
				end
			end
		end

feature -- Conversion

	linear_representation: LINEAR [G]
			-- Representation as a linear structure
		local
			temp: ARRAYED_LIST [G]
			i, j, m, n: INTEGER
		do
			create temp.make (count)
			from
				i := first_row
				m := last_row
			until
				i > m
			loop
				from
					j := first_column
					n := last_column
				until
					j > n
				loop
					temp.extend (item (i, j))
					j := j + 1
				end
				i := i + 1
			end
			Result := temp
		end

feature {SPARSE_SLICE_VIEW2}
	row_base, column_base: INTEGER
	outer_rows, outer_columns: INTEGER
	area: SPECIAL[G]

	freeze
		do
			readers := readers + 1
		end

	melt
		do
			readers := readers - 1
		end

	hide_top (n: INTEGER)
		do
			first_row := first_row + n
		end

	hide_bottom (n: INTEGER)
		do
			last_row := last_row - n
		end

	hide_left (n: INTEGER)
		do
			first_column := first_column + n
		end

	hide_right (n: INTEGER)
		do
			last_column := last_column - n
		end

feature {NONE} -- Implementation

invariant
	readers >= 0

end
