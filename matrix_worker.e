note
	description: "A {MATRIX_WORKER} is a generic superclass for all the workers used on matrices."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	MATRIX_WORKER[G -> NUMERIC]

feature
	output: SLICE2[G]
		deferred
		end

	run
		deferred
		end

end
