note
	description: "A {SHARD2} is a two-dimensional shard."
	author: "Mischael Schill"
	date: "$Date$"
	revision: "$Revision$"

class
	SLICE_VIEW2[G]

inherit
	SPARSE_SLICE_VIEW2[G]
		redefine
			is_empty,
			is_equal
		end

create
	make

feature {NONE} -- Initialization
	make (a_original: separate SLICE2[G])
		note
			ignore_scoop_check: True
		do
			area := a_original.area
			first_row := a_original.first_row
			first_column := a_original.first_column
			last_row := a_original.last_row
			last_column := a_original.last_column
			outer_rows := a_original.outer_rows
			outer_columns := a_original.outer_columns
			row_base := a_original.row_base
			column_base := a_original.column_base
			a_original.freeze
			original := a_original
		end

feature -- Access

	original: separate SLICE2[G]

	item alias "[]" (i, j: INTEGER): G
			-- Entry at index (`i',`j'), if in index interval
		do
			Result := area.item ((i - row_base) * outer_columns + (j - column_base))
		end

feature -- Comparison

	is_equal (other: like Current): BOOLEAN
			-- Is array made of the same items as `other'?
		local
			i, j, m, n: INTEGER
		do
			if other = Current then
				Result := True
			elseif is_comparable_with (other) then
				if object_comparison then
					from
						Result := True
						i := first_row
						m := last_row
					until
						not Result or i > m
					loop
						from
							j := first_column
							n := last_column
						until
							not Result or j > n
						loop
							Result := item (i, j) ~ other.item (i, j)
							j := j + 1
						end
						i := i + 1
					end
				else
					from
						Result := True
						i := first_row
						m := last_row
					until
						not Result or i > m
					loop
						from
							j := first_column
							n := last_column
						until
							not Result or j > n
						loop
							Result := item (i, j) = other.item (i, j)
							j := j + 1
						end
						i := i + 1
					end
				end
			end
		end

	is_comparable_with (other: separate like Current): BOOLEAN
		do
			Result := first_row = other.first_row and last_row = other.last_row
				and first_column = other.first_column and last_column = other.last_column and
				object_comparison = other.object_comparison
		end

feature -- Status report
	first_row, first_column: INTEGER
	last_row, last_column: INTEGER

	valid_indexes (i, j: INTEGER): BOOLEAN
			-- Is (`i',`j') within the bounds of the array?
		do
			Result := (first_row <= i) and (i <= last_row) and (first_column <= j) and (j <= last_column)
		end

	filled_with (a_value: G): BOOLEAN
		local
			i, m: INTEGER
		do
			from
				Result := True
				i := first_row
				m := last_row
			until
				not Result or i > m
			loop
				Result := area.filled_with (a_value,
					(i - row_base) * outer_columns + (first_column - column_base),
					(i - row_base) * outer_columns + (last_column - column_base))
				i := i + 1
			end
		end

	is_empty: BOOLEAN
		do
			Result := count = 0
		end

	has (v: G): BOOLEAN
			-- Does `v' appear in array?
 			-- (Reference or object equality,
			-- based on `object_comparison'.)
		local
			i, j, m, n: INTEGER
		do
			if object_comparison then
				from
					Result := True
					i := first_row
					m := last_row
				until
					not Result or i > m
				loop
					from
						j := first_column
						n := last_column
					until
						not Result or j > n
					loop
						Result := item (i, j) ~ v
						j := j + 1
					end
					i := i + 1
				end
			else
				from
					Result := True
					i := first_row
					m := last_row
				until
					not Result or i > m
				loop
					from
						j := first_column
						n := last_column
					until
						not Result or j > n
					loop
						Result := item (i, j) = v
						j := j + 1
					end
					i := i + 1
				end
			end
		end

	count: INTEGER
		do
			Result := rows * columns
		end

feature -- Element change

	free
		do
			first_row := 1
			first_column := 1
			last_row := 0
			last_column := 0
			row_base := 1
			column_base := 1
			outer_rows := rows
			outer_columns := columns
			create area.make_empty (0)
		end

feature -- Conversion

	linear_representation: LINEAR [G]
			-- Representation as a linear structure
		local
			temp: ARRAYED_LIST [G]
			i, j, m, n: INTEGER
		do
			create temp.make (count)
			from
				i := first_row
				m := last_row
			until
				i > m
			loop
				from
					j := first_column
					n := last_column
				until
					j > n
				loop
					temp.extend (item (i, j))
					j := j + 1
				end
				i := i + 1
			end
			Result := temp
		end

feature {SPARSE_SLICE_VIEW2}
	row_base, column_base: INTEGER
	outer_rows, outer_columns: INTEGER
	area: SPECIAL[G]

feature {NONE} -- Implementation


invariant
	non_negative_area: rows >= 0 and columns >= 0

end
