note
	description: "A {SPARSE_SLICE2} is the common superclass for all two-dimensional slices."
	author: "Mischael Schill"
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SPARSE_SLICE2[G]

inherit
	SPARSE_SLICE_VIEW2[G]
		rename
			free as empty
		end

feature -- Access
	item alias "[]" (i, j: INTEGER): G assign put
			-- Entry at index (`i',`j'), if in index interval
		deferred
		end

feature -- Status report
	readers: INTEGER
		deferred
		end

	frozen is_frozen: BOOLEAN
		do
			Result := readers > 0
		end

	is_modifiable: BOOLEAN
		do
			Result := not is_frozen
		end

feature -- Element change

	put (v: like item; i, j: INTEGER)
			-- Replace (`i',`j')-th entry, if in index interval, by `v'.
		require
			within_bounds: valid_indexes (i, j)
			modifiable: is_modifiable
		deferred
		ensure
			item (i, j) = v
		end

feature {NONE} -- Implementation

end
