note
	description: "A {SPARSE_SHARD} is a general shard."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SPARSE_SLICE_VIEW[G]

inherit
	CONTAINER[G]
		rename
			empty as is_empty
		undefine
			is_equal
		end

feature -- Access

	item alias "[]" (i: INTEGER): G
			-- Entry at index `i'
		require
			within_bounds: valid_index (i)
		deferred
		end

	original: separate SPARSE_SLICE[G]
		deferred
	end

feature -- Measurement

	lower, upper: INTEGER
		deferred
		end

	count: INTEGER
		deferred
		ensure
			Result >= 0
		end

feature -- Status report

	valid_index (i: INTEGER): BOOLEAN
			-- Is `i' within the bounds of the array?
		deferred
		end

	filled_with (a_value: G): BOOLEAN
		deferred
		end

feature -- Comparison

	frozen is_top_of (a_other: separate like Current): BOOLEAN
		do
			Result := upper = a_other.lower - 1
		end

	frozen is_bottom_of (a_other: separate like Current): BOOLEAN
		do
			Result := a_other.is_top_of (Current)
		end

	frozen is_adjacent (a_other: separate like Current): BOOLEAN
		do
			Result :=
				is_top_of (a_other) or
				is_bottom_of (a_other) 
		end

feature -- Element change

	free
		deferred
		ensure
			empty: count = 0
		end

invariant
	bounds_consistent: lower <= upper + 1

end
