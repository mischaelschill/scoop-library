note
	description: "A {SPARSE_SLICE_VIEW2} is the most general form of a two dimensional slice view."
	author: "Mischael Schill"
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SPARSE_SLICE_VIEW2[G]

inherit
	CONTAINER[G]
		rename
			empty as is_empty
		end


feature -- Access

	item alias "[]" (i, j: INTEGER): G
			-- Entry at index (`i',`j'), if in index interval
		require
			within_bounds: valid_indexes (i, j)
		deferred
		end

	original: separate SPARSE_SLICE2[G]
		deferred
	end

feature -- Measurement

	first_row, first_column: INTEGER
		deferred
		end

	last_row, last_column: INTEGER
		deferred
		end

	frozen rows: INTEGER
		do
			Result := last_row - first_row + 1
		end

	frozen columns: INTEGER
		do
			Result := last_column - first_column + 1
		end

	count: INTEGER
		deferred
		ensure
			Result >= 0
		end

feature -- Status report

	valid_indexes (i, j: INTEGER): BOOLEAN
			-- Is (`i',`j') within the bounds of the array?
		deferred
		end

	filled_with (a_value: G): BOOLEAN
		deferred
		end

feature -- Comparison

	frozen is_top_of (a_other: separate like Current): BOOLEAN
		do
			Result := last_row = a_other.first_row - 1 and
				first_column = a_other.first_column and
				last_column = a_other.last_column
		end

	frozen is_bottom_of (a_other: separate like Current): BOOLEAN
		do
			Result := a_other.is_top_of (Current)
		end

	frozen is_left_of (a_other: separate like Current): BOOLEAN
		do
			Result := last_column = a_other.first_column - 1 and
				first_row = a_other.first_row and
				last_row = a_other.last_row
		end

	frozen is_right_of (a_other: separate like Current): BOOLEAN
		do
			Result := a_other.is_left_of (Current)
		end

	frozen is_adjacent (a_other: separate like Current): BOOLEAN
		do
			Result :=
				is_top_of (a_other) or
				is_bottom_of (a_other) or
				is_left_of (a_other) or
				is_right_of (a_other)
		end

feature -- Element change

	free
		deferred
		ensure
			empty: rows = 0 and columns = 0
		end

invariant
	rows_consistent: rows = last_row - first_row + 1
	columns_consistent: columns = last_column - first_column + 1

end
