note
	description: "A {MATRIX_MULTIPLIER} is used to multiply matrices using multiple processors."
	author: "Mischael Schill"
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_MULTIPLIER[G -> NUMERIC]

inherit
	MATRIX_WORKER[G]
		rename
			run as multiply
		end

create
	make

feature {NONE} -- Initialization
	make (a_left, a_right, a_output: separate SLICE2[G]; a_rows: INTEGER)
		do
			create left.make(a_left)
			create right.make(a_right)
			create output.slice_top (a_rows.min (a_output.rows), a_output)
		end


feature -- Access
	left, right: SLICE_VIEW2[G]
	output: SLICE2[G]

feature -- Element change
	multiply
		local
			k, i, j, m, n, o: INTEGER
			ou: SLICE2[G]
			l,r: SLICE_VIEW2[G]
		do
			ou := output
			l := left
			r := right
			from
				i := output.first_row
				m := output.last_row
			until
				i > m
			loop
				from
					j := output.first_column
					n := output.last_column
				until
					j > n
				loop
					from
						k := left.first_column
						o := left.last_column
					until
						k > o
					loop
						ou[i, j] := ou[i, j] + l[i, k] * r[k, j]
						k := k + 1
					end
					j := j + 1
				end
				i := i + 1
			end
			left.free
			right.free
		end

end
