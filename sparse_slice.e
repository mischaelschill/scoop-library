note
	description: "{SPARSE_SLICE} is a general superclass for all kinds of slices."
	author: "Mischael Schill"
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SPARSE_SLICE[G]

inherit
	SPARSE_SLICE_VIEW[G]
		rename
			free as empty
		undefine
			is_empty
		end

feature -- Access
	item alias "[]" (i: INTEGER): G assign put
			-- Entry at index `i'
		deferred
		end

feature -- Status report
	readers: INTEGER
		deferred
		end

	frozen is_frozen: BOOLEAN
		do
			Result := readers > 0
		end

	is_modifiable: BOOLEAN
		do
			Result := not is_frozen
		end

feature -- Element change

	put (v: like item; i: INTEGER)
			-- Replace `i'-th entry, if in index interval, by `v'.
		require
			within_bounds: valid_index (i)
			modifiable: is_modifiable
		deferred
		ensure
			item (i) = v
		end
end
