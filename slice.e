class SLICE [G]

inherit
	SPARSE_SLICE[G]
		undefine
			copy
		end

create
	make_empty,
	make_from_array,
	make_filled,
	slice_head,
	slice_tail,
	merge

feature {NONE} -- Initialization

	make_empty
			-- Allocate empty array starting at `1'.
		do
			empty
		ensure
			lower_set: lower = 1
			upper_set: upper = 0
			base_set: base = lower
			items_set: all_default
		end

	make_from_array (a_original: separate ARRAY[G])
			-- Create a new slice by copying all elements of `a_original'
		note
			ignore_scoop_check: True
		local
			a: SPECIAL[G]
		do
			a := a_original.area
			lower := a_original.lower
			upper := a_original.upper
			base := lower
			create area.make_empty (count)
			area.copy_data (a, 0, 0, count)
		ensure
			lower_set: lower = a_original.lower
			upper_set: upper = a_original.upper
			base_set: base = lower
		end

	slice_head (n: INTEGER; a_original: separate SLICE[G])
		note
			ignore_scoop_check: True
		require
			within_bounds: n >= 0 and n <= a_original.count
			not a_original.is_frozen
		do
			lower := a_original.lower
			upper := a_original.lower + n - 1
			base := a_original.base
			area := a_original.area
			a_original.slice_lower (n)
		ensure
			a_original.count = old a_original.count - n
			a_original.lower = old a_original.lower + n
			lower = old a_original.lower
			count = n
		end

	slice_tail (n: INTEGER; a_original: separate SLICE[G])
		note
			ignore_scoop_check: True
		require
			within_bounds: n >= 0 and n <= a_original.count
			not a_original.is_frozen
		do
			lower := a_original.upper - n + 1
			upper := a_original.upper
			base := a_original.base
			area := a_original.area
			a_original.slice_upper (n)
		ensure
			a_original.count = old a_original.count - n
			a_original.upper = old a_original.upper - n
			upper = old a_original.upper
			count = n
		end

	make_filled (a_default_value: G; a_lower, a_upper: INTEGER)
			-- Allocate array; set index interval to
			-- `a_lower' .. `a_upper'; set all values to default.
			-- (Make array empty if `a_lower' = `a_upper' + 1).
		require
			valid_bounds: a_upper >= a_lower - 1
		do
			lower := a_lower
			upper := a_upper
			base := a_lower
			create area.make_filled (a_default_value, count)
		ensure
			lower_set: lower = a_lower
			upper_set: upper = a_upper
			base_set: base = lower
			items_set: filled_with (a_default_value)
		end

	merge (a_one, a_another: separate like Current)
		note
			ignore_scoop_check: True
		require
			not a_one.is_frozen
			not a_another.is_frozen
			adjacent: a_one.is_adjacent (a_another)
		do

			if a_one.is_top_of (a_another) then
				lower := a_one.lower
				upper := a_another.upper
			else
				lower := a_another.lower
				upper := a_one.upper
			end

			if
				a_one.area = a_another.area and
				a_one.base = a_another.base
			then
				area := a_one.area
				base := a_one.base
			else
				base := lower
				create area.make_empty (count)
				area.copy_data (
					a_one.area,
					a_one.lower - a_one.base, lower - base, a_one.count)
				area.copy_data (
					a_another.area,
					a_another.lower - a_another.base, lower - base, a_another.count)
			end
			a_another.empty
			a_one.empty
		end

feature -- Access

	item alias "[]", at alias "@" (i: INTEGER): G assign put
			-- Entry at index `i', if in index interval
		do
			Result := area.item (i - base)
		end

	original: separate SPARSE_SLICE[G]
		do
			Result := Current
		end

feature -- Measurement

	lower: INTEGER
			-- Minimum index

	upper: INTEGER
			-- Maximum index

	base: INTEGER
			-- Base relative to the area, used when dividing

	count, capacity: INTEGER
			-- Number of available indices
		do
			Result := upper - lower + 1
		ensure then
			consistent_with_bounds: Result = upper - lower + 1
		end

	occurrences (v: G): INTEGER
			-- Number of times `v' appears in structure
		local
			i: INTEGER
		do
			if object_comparison then
				from
					i := lower
				until
					i > upper
				loop
					if item (i) ~ v then
						Result := Result + 1
					end
					i := i + 1
				end
			else
				from
					i := lower
				until
					i > upper
				loop
					if item (i) = v then
						Result := Result + 1
					end
					i := i + 1
				end
			end
		end

	has (v: G): BOOLEAN
			-- Does `v' appear in array?
 			-- (Reference or object equality,
			-- based on `object_comparison'.)
		local
			i: INTEGER
		do
			if object_comparison and v /= Void then
				from
					i := lower
				until
					i > upper or Result
				loop
					Result := item (i) ~ v
					i := i + 1
				end
			else
				from
					i := lower
				until
					i > upper or Result
				loop
					Result := item (i) = v
					i := i + 1
				end
			end
		end

	index_set: INTEGER_INTERVAL
			-- Range of acceptable indexes
		do
			create Result.make (lower, upper)
		ensure then
			same_count: Result.count = count
			same_bounds:
				((Result.lower = lower) and (Result.upper = upper))
		end

feature -- Comparison

	is_equal (other: like Current): BOOLEAN
			-- Is array made of the same items as `other'?
		local
			i: INTEGER
		do
			if other = Current then
				Result := True
			elseif lower = other.lower and then upper = other.upper and then
				object_comparison = other.object_comparison
			then
				if object_comparison then
					from
						Result := True
						i := lower
					until
						not Result or i > upper
					loop
						Result := item (i) ~ other.item (i)
						i := i + 1
					end
				else
					Result := area.same_items (other.area, lower - base, other.lower - other.base, count)
				end
			end
		end

feature -- Status report

	all_default: BOOLEAN
			-- Are all items set to default values?
		do
			if count > 0 then
				Result := ({G}).has_default and then area.filled_with (({G}).default, lower - base, upper - base)
			else
				Result := True
			end
		end

	filled_with (v: G): BOOLEAN
			-- Are all itms set to `v'?
		do
			Result := area.filled_with (v, lower - base, upper - base)
		end

	full: BOOLEAN
			-- Is structure filled to capacity? (Answer: yes)
		do
			Result := True
		end

	same_items (other: like Current): BOOLEAN
			-- Do `other' and Current have same items?
		require
			other_not_void: other /= Void
		do
			if count = other.count then
				Result := area.same_items (other.area, lower - base, other.lower - other.base, count)
			end
		end

	valid_index (i: INTEGER): BOOLEAN
			-- Is `i' within the bounds of the array?
		do
			Result := (lower <= i) and then (i <= upper)
		end

	extendible: BOOLEAN
			-- May items be added?
			-- (Answer: no.)
		do
			Result := False
		end

	prunable: BOOLEAN
			-- May items be removed? (Answer: no.)
		do
			Result := False
		end

	valid_index_set: BOOLEAN
		do
			Result := index_set.count = count
		end

	is_empty: BOOLEAN
		do
			Result := count = 0
		end

feature -- Iteration

	do_all (action: PROCEDURE [ANY, TUPLE [G]])
			-- Apply `action' to every item, from first to last.
			-- Semantics not guaranteed if `action' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
		do
			area.do_all_in_bounds (action, lower - base, upper - base)
		end

	do_if (action: PROCEDURE [ANY, TUPLE [G]]; test: FUNCTION [ANY, TUPLE [G], BOOLEAN])
			-- Apply `action' to every item that satisfies `test', from first to last.
			-- Semantics not guaranteed if `action' or `test' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
			test_not_void: test /= Void
		do
			area.do_if_in_bounds (action, test, lower - base, upper - base)
		end

	there_exists (test: FUNCTION [ANY, TUPLE [G], BOOLEAN]): BOOLEAN
			-- Is `test' true for at least one item?
		require
			test_not_void: test /= Void
		do
			Result := area.there_exists_in_bounds (test, lower - base, upper - base)
		end

	for_all (test: FUNCTION [ANY, TUPLE [G], BOOLEAN]): BOOLEAN
			-- Is `test' true for all items?
		require
			test_not_void: test /= Void
		do
			Result := area.for_all_in_bounds (test, lower - base, upper - base)
		end

	do_all_with_index (action: PROCEDURE [ANY, TUPLE [G, INTEGER]])
			-- Apply `action' to every item, from first to last.
			-- `action' receives item and its index.
			-- Semantics not guaranteed if `action' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		local
			i: INTEGER
		do
			from
				i := lower
			until
				i > upper
			loop
				action.call ([item (i), i])
			end
		end

	do_if_with_index (action: PROCEDURE [ANY, TUPLE [G, INTEGER]]; test: FUNCTION [ANY, TUPLE [G, INTEGER], BOOLEAN])
			-- Apply `action' to every item that satisfies `test', from first to last.
			-- `action' and `test' receive the item and its index.
			-- Semantics not guaranteed if `action' or `test' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		local
			i: INTEGER
		do
			from
				i := lower
			until
				i > upper
			loop
				if test.item ([item (i), i]) then
					action.call ([item (i), i])
				end
				i := i + 1
			end
		end

feature -- Conversion
	linear_representation: LINEAR [G]
			-- Representation as a linear structure
		local
			temp: ARRAYED_LIST [G]
			i: INTEGER
		do
			create temp.make (capacity)
			from
				i := lower
			until
				i > upper
			loop
				temp.extend (item (i))
				i := i + 1
			end
			Result := temp
		end

feature {SLICE_VIEW} -- Status change (shared read)
	readers: INTEGER

	freeze
		do
			readers := readers + 1
		end

	melt
		do
			readers := readers - 1
		end

feature -- Element change

	put (v: like item; i: INTEGER)
			-- Replace `i'-th entry, if in index interval, by `v'.
		do
			check not is_frozen end
			area.put (v, i - base)
		end

	fill_with (v: G)
			-- Set items between `lower' and `upper' with `v'.
		require
			not is_frozen
		do
			area.fill_with (v, lower - base, upper - base)
		ensure
			count_definition: count = old count
			filled: filled_with (v)
		end

	subcopy (other: like Current; start_pos, end_pos, index_pos: INTEGER)
			-- Copy items of `other' within bounds `start_pos' and `end_pos'
			-- to current array starting at index `index_pos'.
		require
			other_not_void: other /= Void
			valid_start_pos: start_pos >= other.lower
			valid_end_pos: end_pos <= other.upper
			valid_bounds: start_pos <= end_pos + 1
			valid_index_pos: index_pos >= lower
			enough_space: (upper - index_pos) >= (end_pos - start_pos)
		do
			area.copy_data (other.area, start_pos - other.base, index_pos - base, end_pos - start_pos + 1)
		ensure
			-- copied: forall `i' in 0 .. (`end_pos'-`start_pos'),
			--     item (index_pos + i) = other.item (start_pos + i)
		end

feature -- Removal
	clear_all
			-- Reset all items to default values.
		require
			has_default: ({G}).has_default
			not is_frozen
		do
			area.fill_with (({G}).default, lower - base, upper - base)
		ensure
			stable_lower: lower = old lower
			stable_upper: upper = old upper
			default_items: all_default
		end

	rebase (a_lower: like lower)
			-- Without changing the actual content of `Current' we set `lower' to `a_lower'
			-- and `upper' accordingly to `a_lower + count - 1'.
		do
			base := base + (a_lower - lower)
			upper := upper + (a_lower - lower)
			lower := a_lower
		ensure
			lower_set: lower = a_lower
			upper_set: upper = a_lower + old count - 1
		end

	empty
			-- Allocate empty array starting at `1'.
		do
			lower := 1
			upper := 0
			base := 1
			create area.make_empty (0)
		end

feature -- Duplication

	copy (other: like Current)
			-- Reinitialize by copying all the items of `other'.
			-- (This is also used by `clone'.)
		local
			i, j: INTEGER
		do
			if not is_frozen then
				from
					i := lower
					j := other.lower
				until
					i > upper or j > other.upper
				loop
					put (other[j], i)
				end
			end
		end

feature {NONE} -- Implementation



feature {SLICE, SLICE_VIEW}
	area: SPECIAL[G]

	slice_upper (c: INTEGER)
		do
			upper := upper - c
		end

	slice_lower (c: INTEGER)
		do
			lower := lower + c
		end

invariant
	area_exists: area /= Void
	consistent_size: count = upper - lower + 1
	non_negative_count: count >= 0
	index_set_has_same_count: valid_index_set
-- Internal discussion haven't reached an agreement on this invariant
--	index_set_has_same_bounds: ((index_set.lower = lower) and
--				(index_set.upper = lower + count - 1))

note
	copyright: "Copyright (c) 1984-2012, Eiffel Software and others"
	license:   "Eiffel Forum License v2 (see http://www.eiffel.com/licensing/forum.txt)"
	source: "[
			Eiffel Software
			5949 Hollister Ave., Goleta, CA 93117 USA
			Telephone 805-685-1006, Fax 805-685-6869
			Website http://www.eiffel.com
			Customer support http://support.eiffel.com
		]"

end
