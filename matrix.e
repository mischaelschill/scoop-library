note
	description: "A {MATRIX} is a matrix that is backed by a two dimensional slice."
	author: "Mischael Schill"
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX[G -> NUMERIC]

inherit ANY
	redefine
		default_create
	end

create
	default_create,
	make,
	make_empty,
	make_default,
	make_filled,
	slice_top,
	slice_bottom,
	slice_left,
	slice_right,
	merge

feature {NONE} -- Initialization
	default_create
		do
			make_empty
		end

	make (a_data: like data)
		do
			data := a_data
		end

	make_empty
		do
			create data.make_empty
		end

	make_filled (a_default: G; a_rows, a_columns: INTEGER)
		do
			create data.make_filled (a_default, a_rows, a_columns)
		end

	make_default (a_rows, a_columns: INTEGER)
		do
			create data.make_filled (({G}).default, a_rows, a_columns)
		end

	slice_top (n: INTEGER; a_original: separate like Current)
		-- Slices off the first `n' rows from `a_original'
		require
			within_bounds: n > 0 and n <= a_original.rows
			original_is_modifiable:  a_original.is_modifiable
		do
			create data.slice_top (n, a_original.data)
		ensure
			a_original.first_row = old a_original.first_row + n
			first_row = old a_original.first_row
			first_column = a_original.first_column
			last_row = a_original.first_row - 1
			last_column = a_original.last_column
		end

	slice_bottom (n: INTEGER; a_original: separate like Current)
		-- Slices off the last `n' rows from `a_original'
		require
			within_bounds: n > 0 and n <= a_original.rows
			original_is_modifiable: a_original.is_modifiable
		do
			create data.slice_bottom (n, a_original.data)
		ensure
			a_original.last_row = old a_original.last_row - n
			first_row = a_original.last_row + 1
			first_column = a_original.first_column
			last_row = old a_original.last_row
			last_column = a_original.last_column
		end

	slice_left (n: INTEGER; a_original: separate like Current)
		-- Slices off the first `n' columns from `a_original'
		require
			within_bounds: n > 0 and n <= a_original.columns
			original_is_modifiable: a_original.is_modifiable
		do
			create data.slice_left (n, a_original.data)
		ensure
			a_original.first_row = old a_original.first_row + n
			first_row = a_original.first_row
			first_column = old a_original.first_column
			last_row = a_original.last_row
			last_column = a_original.last_column - 1
		end

	slice_right (n: INTEGER; a_original: separate like Current)
		-- Slices off the last `n' columns from `a_original'
		require
			within_bounds: n > 0 and n <= a_original.columns
			original_is_modifiable: a_original.is_modifiable
		do
			create data.slice_right (n, a_original.data)
		ensure
			a_original.first_row = old a_original.first_row + n
			first_row = a_original.first_row
			first_column = a_original.last_column + 1
			last_row = a_original.last_row
			last_column = old a_original.last_column
		end

	merge (a_one, a_another: separate like Current)
		require
			a_one.is_modifiable
			a_another.is_modifiable
			adjacent: a_one.is_adjacent (a_another)
		local
			l_data: SLICE2[G]
		do
			create l_data.merge (a_one.data, a_another.data)
			data := l_data
		end


feature -- Access
	data: SLICE2[G]

	item alias "[]" (i, j: INTEGER): G assign put
		require
			data.valid_indexes (i, j)
		do
			Result := data[i, j]
		end

feature -- Element change
	put (v: G; i, j: INTEGER)
		require
			within_bounds: data.valid_indexes (i, j)
			modifiable: data.is_modifiable
		do
			data.put (v, i, j)
		ensure
			Current[i, j] = v
		end

feature -- Status report
	is_modifiable: BOOLEAN
		do
			if attached {SPARSE_SLICE2[G]} data as d then
				Result := d.is_modifiable
			else
				Result := False
			end
		end

feature -- Status report
	workers: INTEGER assign set_workers

feature -- Measurement
	first_row: INTEGER
		do
			Result := data.first_row
		end

	last_row: INTEGER
		do
			Result := data.last_row
		end

	first_column: INTEGER
		do
			Result := data.first_column
		end

	last_column: INTEGER
		do
			Result := data.last_column
		end

	rows: INTEGER
		do
			Result := data.rows
		end

	columns: INTEGER
		do
			Result := data.columns
		end

	is_adjacent (a_other: separate like Current): BOOLEAN
		do
			Result := data.is_adjacent (a_other.data)
		end

feature -- Status change

	set_workers (n: INTEGER)
		require
			n >= 0
		do
			workers := n
		ensure
			workers = n
		end

feature -- Operations
	multiply alias "*" (other: separate MATRIX[G]): MATRIX[G]
		local
			separate_worker: separate MATRIX_MULTIPLIER[G]
			output: SLICE2[G]
		do
			create output.make_filled (({G}).default, other.columns, rows)
			output := separate_multiply (workers, other.data, output)
			create Result.make (output)
		end

feature {NONE} -- Implementation

	separate_multiply (w: INTEGER; a_right: separate SLICE2[G]; a_result: SLICE2[G]): like a_result
		local
			separate_worker: separate MATRIX_MULTIPLIER[G]
			temp: like a_result
		do
			if w > 0 then
				create separate_worker.make (data, a_right, a_result, (rows / workers).ceiling.min(rows))
				separate_run (separate_worker)
				temp := separate_multiply (w - 1, a_right, a_result)
				create Result.merge (
					separate_output(separate_worker),
					temp)
			else
				Result := a_result;
			end
		end

	separate_run (a_worker: separate MATRIX_WORKER[G])
		do
			a_worker.run
		end

	separate_output (a_worker: separate MATRIX_WORKER[G]): separate SLICE2[G]
		do
			Result := a_worker.output
		end

invariant
	non_negative_size: rows >= 0 and columns >= 0
	rows_definition: rows = last_row - first_row + 1
	columns_definition: columns = last_column - first_column + 1
end
